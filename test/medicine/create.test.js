import supertest from 'supertest'
import app from 'app'
import sinon from 'sinon'
import jwt from 'jsonwebtoken'
import config from 'config'

import MedicineFactory from 'test/factories/medicine'
import MedicineService from 'services/medicine'

describe('POST /api/v1/medicines', () => {
  const request = supertest(app)
  const endpoint = '/api/v1/medicines'
  const token = jwt.sign({}, config.jwtPrivateKey)

  afterEach(() => {
    sinon.restore()
  })

  describe('Unauthorized request', () => {
    describe('Missing token in header', () => {
      it('returns 401', async () => {
        const res = await request.post(endpoint).send({
          name: 'Medicine 1',
          price: 10000
        })
        expect(res.status).toEqual(401)
      })
    })

    describe('Token is invalid', () => {
      it('returns 401', async () => {
        const invalidToken = jwt.sign({}, 'fake-private')
        const res = await request.post(endpoint).set('token', invalidToken).send({
          name: 'Medicine 1',
          price: 10000
        })
        expect(res.status).toEqual(401)
      })
    })
  })

  describe('Create medicine successfully', () => {
    it('returns created medicine', async () => {
      const res = await request.post(endpoint).set('token', token).send({
        name: 'Medicine 1',
        price: 10000
      })

      expect(res.status).toEqual(201)
      expect(res.body.name).toEqual('Medicine 1')
      expect(res.body.price).toEqual('10000')
    })
  })

  describe('Create medicine failed', () => {
    describe('A medicine with given name already existed', () => {
      beforeEach(async () => {
        await MedicineFactory.create({ name: 'Medicine 1' })
      })

      it('returns status 400', async () => {
        const res = await request.post(endpoint).set('token', token).send({
          name: 'Medicine 1',
          price: 10000
        })

        expect(res.status).toEqual(400)
      })
    })

    describe('Server error', () => {
      beforeEach(() => {
        sinon.stub(MedicineService, 'insertMedicine').throws()
      })

      it('returns status 500', async () => {
        const res = await request.post(endpoint).set('token', token).send({
          name: 'Medicine 1',
          price: 10000
        })
        expect(res.status).toEqual(500)
      })
    })
  })
})
