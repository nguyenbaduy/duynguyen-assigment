'use strict'

module.exports = (sequelize, DataTypes) => {
  const Medicine = sequelize.define('Medicine', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING
    },
    price: {
      allowNull: false,
      type: DataTypes.DECIMAL
    },
    description: {
      allowNull: true,
      type: DataTypes.TEXT
    },
  })

  return Medicine
}
