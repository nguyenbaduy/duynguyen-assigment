import { Medicine } from 'models'

class MedicineService {
  constructor() {
    this.model = Medicine
  }

  async insertMedicine(medicineInfo) {
    try {
      const medicine = await this.model.create(medicineInfo)
      return {
        status: 201,
        data: medicine
      }
    } catch (error) {
      return {
        status: 400,
        error: error.message
      }
    }
  }
}

export default new MedicineService()
