import {
  Router
} from 'express'
import {
  create,
  list,
  get,
  remove,
  update
} from 'controllers/medicine'
import { authHanlder } from 'middlewares'

const router = new Router()

router.post('', authHanlder, create)

export default router
