import {
  Router
} from 'express'

import jwt from 'jsonwebtoken'

import config from 'config'

import MedicineRouter from './medicine'

const router = Router()

router.get('/', (req, res) => {
  res.status(200).send('Hello')
})

router.post('/login', (req, res) => {
  const token = jwt.sign({}, config.jwtPrivateKey)
  res.status(200).json({ token })
})

router.use('/medicines', MedicineRouter)

export default router
