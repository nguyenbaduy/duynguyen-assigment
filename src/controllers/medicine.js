import MedicineService from 'services/medicine'

export const create = async (req, res) => {
  const { name, price, description, ...other } = req.body
  try {
    const { status, data, error } = await MedicineService.insertMedicine({ name, price, description })
    return res.status(status).json(error || data)
  } catch (error) {
      res.status(500).send()
    }
}
