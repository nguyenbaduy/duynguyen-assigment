import jwt from 'jsonwebtoken'

import config from 'config'

export const authHanlder = (req, res, next) => {
  const { token } = req.headers
  if (!token) {
    res.status(401).send('Unauthorized')
    return
  }

  try {
    const _ = jwt.verify(token, config.jwtPrivateKey)
    next()
  } catch (error) {
    res.status(401).send('Unauthorized')
  }
}
