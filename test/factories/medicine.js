import {
  factory
} from 'factory-girl'

import {
  Medicine
} from 'models'

factory.define('Medicines', Medicine, {
  name: factory.chance('name'),
  price: factory.chance('integer')
})

export default {
  create: (attrs = {}) => factory.create('Medicines', attrs),
  createMany: (count = 2, attrs = {}) => factory.createMany('Medicines', count, attrs),
  attrs: (attrs = {}) => factory.attrs('Medicines', attrs)
}
